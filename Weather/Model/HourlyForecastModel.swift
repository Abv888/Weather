//
//  HourlyForecastModel.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 28.09.2023.
//

import Foundation

struct HourlyForecastModel {
	let currentWeatherSummary: String
	let hourlySnapshots: [WeatherSnapshot]
}
