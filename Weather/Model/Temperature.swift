//
//  Temperature.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 28.09.2023.
//

import Foundation

enum Temperature {
	case C(_ value: Int)
	case F(_ value: Int)
	
	static let degreeSymbol = "°"
	
	var valueInFahrenheit: Int {
		switch self {
		case .C(let value): return Int(((Double(value) * 1.8) + 32.0).rounded())
		case .F(let value): return value
		}
	}
	
	var valueInCelsius: Int {
		switch self {
		case .C(let value): return value
		case .F(let value): return Int(((Double(value) - 32.0) / 1.8).rounded())
		}
	}
	
	var celsiusString: String { return "\(valueInCelsius)\(Temperature.degreeSymbol)" }
	var fahrenheitString: String { return "\(valueInFahrenheit)\(Temperature.degreeSymbol)" }
}
