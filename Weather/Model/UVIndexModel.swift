//
//  UVIndexModel.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 29.09.2023.
//

import Foundation

struct UVIndexModel {
	let uvIndexValue: Int
	let indexRating: String
	var progressViewValue: Double
	let recomendation: String
}
