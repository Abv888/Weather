//
//  WeatherCondition.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 28.09.2023.
//

import SwiftUI

enum WeatherCondition: CustomStringConvertible {
	
	case sunrise, sunset, clear, mostlyClear, partlyCloudy, haze, fog, windy, breezy, cloudy, thunderstorm, rain, heavyRain, drizzle, freezingDreezle, snow, heavySnow, blizzard, freezingRain, sleet, wintryMix, clearNight, mostlyClearNight, partlyCloudyNight, drizzleNight
	
	var description: String {
		switch self {
		case .sunrise:
			return "Sunrise"
		case .sunset:
			return "Sunset"
		case .clear:
			return "Clear"
		case .mostlyClear:
			return "Mostly Clear"
		case .partlyCloudy:
			return "Partly Cloudy"
		case .haze:
			return "Haze"
		case .fog:
			return "Fog"
		case .windy:
			return "Windy"
		case .breezy:
			return "Breezy"
		case .cloudy:
			return "Cloudy"
		case .thunderstorm:
			return "Thunderstorm"
		case .rain:
			return "Rain"
		case .heavyRain:
			return "Heavy Rain"
		case .drizzle:
			return "Drizzle"
		case .freezingDreezle:
			return "Freezing Drizzle"
		case .snow:
			return "Snow"
		case .heavySnow:
			return "Heavy Snow"
		case .blizzard:
			return "Blizzard"
		case .freezingRain:
			return "Freezing Rain"
		case .sleet:
			return "Sleet"
		case .wintryMix:
			return "Wintry Mix"
		case .clearNight:
			return "Clear"
		case .mostlyClearNight:
			return "Mostly Clear"
		case .partlyCloudyNight:
			return "Partly Cloudy"
		case .drizzleNight:
			return "Drizzle"
		}
	}
	
	//TODO: Add tint/accentColor
	var systemImageName: String {
		switch self {
		case .sunrise:
			return "sunrise.fill"
		case .sunset:
			return "sunset.fill"
		case .clear:
			return "sun.max.fill"
		case .mostlyClear:
			return "sun.max.fill"
		case .partlyCloudy:
			return "cloud.sun.fill"
		case .haze:
			return "sun.haze.fill"
		case .fog:
			return "cloud.fog.fill"
		case .windy:
			return "wind"
		case .breezy:
			return "wind"
		case .cloudy:
			return "cloud.fill"
		case .thunderstorm:
			return "cloud.bolt.rain.fill"
		case .rain:
			return "cloud.rain.fill"
		case .heavyRain:
			return "cloud.heavyrain.fill"
		case .drizzle:
			return "cloud.drizzle.fill"
		case .freezingDreezle:
			return "cloud.drizzle.fill"
		case .snow:
			return "snowflake"
		case .heavySnow:
			return "cloud.snow.fill"
		case .blizzard:
			return "cloud.snow.fill"
		case .freezingRain:
			return "cloud.sleet.fill"
		case .sleet:
			return "cloud.sleet.fill"
		case .wintryMix:
			return "cloud.sleet.fill"
		case .clearNight:
			return "moon.stars.fill"
		case .mostlyClearNight:
			return "moon.stars.fill"
		case .partlyCloudyNight:
			return "cloud.moon.fill"
		case .drizzleNight:
			return "cloud.moon.rain.fill"
		}
	}
	
	var weatherImage: some View {
		let image = Image(systemName: systemImageName)
		switch self {
		case .sunrise:
			return image.foregroundColor(.yellow)
		case .sunset:
			return image.foregroundColor(.yellow)
		case .clear:
			return image.foregroundColor(.yellow)
		case .mostlyClear:
			return image.foregroundColor(.yellow)
		case .partlyCloudy:
			return image.foregroundColor(.yellow)
		case .haze:
			return image.foregroundColor(.yellow)
		case .fog:
			return image.foregroundColor(.yellow)
		case .windy:
			return image.foregroundColor(.yellow)
		case .breezy:
			return image.foregroundColor(.yellow)
		case .cloudy:
			return image.foregroundColor(.yellow)
		case .thunderstorm:
			return image.foregroundColor(.yellow)
		case .rain:
			return image.foregroundColor(.yellow)
		case .heavyRain:
			return image.foregroundColor(.yellow)
		case .drizzle:
			return image.foregroundColor(.yellow)
		case .freezingDreezle:
			return image.foregroundColor(.yellow)
		case .snow:
			return image.foregroundColor(.yellow)
		case .heavySnow:
			return image.foregroundColor(.yellow)
		case .blizzard:
			return image.foregroundColor(.yellow)
		case .freezingRain:
			return image.foregroundColor(.yellow)
		case .sleet:
			return image.foregroundColor(.yellow)
		case .wintryMix:
			return image.foregroundColor(.yellow)
		case .clearNight:
			return image.foregroundColor(.yellow)
		case .mostlyClearNight:
			return image.foregroundColor(.yellow)
		case .partlyCloudyNight:
			return image.foregroundColor(.yellow)
		case .drizzleNight:
			return image.foregroundColor(.yellow)
		}
	}
}
