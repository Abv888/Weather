//
//  WeatherSnapshot.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 28.09.2023.
//

import Foundation

struct WeatherSnapshot {
	let time: Date
	let condition: WeatherCondition
	let temperature: Temperature
}
