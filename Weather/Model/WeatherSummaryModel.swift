//
//  WeatherSummaryModel.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 27.09.2023.
//

import SwiftUI

struct WeatherSummaryModel {
	let locationCityName: String
	let currentTemperature: Temperature
	let weatherCondition: WeatherCondition
	let lowestDayTemperature: Temperature
	let highestDayTemperature: Temperature
}
