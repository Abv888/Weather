//
//  WeatherWidgetModel.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 30.09.2023.
//

import SwiftUI

struct WeatherWidgetModel {
	@State var headerSystemImageName: String?
	@State var headerText: String?
	@State var mainInfo: String?
	@State var supportInfo: String?
//	@State var viewData: (any View)?
	@State var information: String?
}

//extension WeatherWidgetModel {
//	init(headerSystemImageName: String? = nil,
//		 headerText: String? = nil,
//		 mainInfo: String? = nil,
//		 supportInfo: String? = nil,
////		 @ViewBuilder viewData: @escaping () -> some View,
//		 information: String? = nil) {
//		self.headerSystemImageName = headerSystemImageName
//		self.headerText = headerText
//		self.mainInfo = mainInfo
//		self.supportInfo = supportInfo
////		self.viewData = viewData()
//		self.information = information
//	}
//	
////	init(headerSystemImageName: String? = nil,
////		 headerText: String? = nil,
////		 mainInfo: String? = nil,
////		 supportInfo: String? = nil,
////		 information: String? = nil) where any viewData == EmptyView {
////		self.init(headerSystemImageName: headerSystemImageName,
////				  headerText: headerText,
////				  mainInfo: mainInfo,
////				  supportInfo: supportInfo,
////				  viewData: nil,
////				  information: information)
////	}
//}
