//
//  RangedProgressView.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 22.09.2023.
//

import SwiftUI

struct RangedProgressView: ProgressViewStyle {
	let range: ClosedRange<Double>
	let bacgroundColor: Color
	let foregroundColors: [Color]
	
	func makeBody(configuration: Configuration) -> some View {
		return GeometryReader { proxy in
			ZStack(alignment: .leading) {
				Capsule()
					.fill(bacgroundColor)
				Capsule()
					.fill(LinearGradient(colors: foregroundColors, startPoint: .leading, endPoint: .trailing))
					.frame(width: proxy.size.width * fillWidthScale)
					.offset(x: proxy.size.width * range.lowerBound)
				if configuration.fractionCompleted != nil {
					Circle()
						.foregroundColor(bacgroundColor)
						.frame(width: proxy.size.width + 5, height: proxy.size.height + 5)
						.position(x: proxy.size.width * (configuration.fractionCompleted ?? 0.0), y: proxy.size.height/2.0)
					Circle()
						.foregroundColor(.white)
						.position(x: proxy.size.width * (configuration.fractionCompleted ?? 0.0), y: proxy.size.height/2.0)
				}
			}
			.clipped()
		}
	}
	
	var fillWidthScale: Double {
		let normalizedRange = range.upperBound - range.lowerBound
		return Double(normalizedRange)
	}
}

#Preview {
	ScrollView {
		VStack {
			ForEach(0...10, id: \.self) { value in
				let range = 0.0...(Double(value) / 10.0)
				ProgressView(value: 0.5)
					.frame(height: 10)
					.progressViewStyle(RangedProgressView(range: range, bacgroundColor: .blue, foregroundColors: [.green, .yellow, .orange, .red]))
					.padding()
			}
			ForEach(0...10, id: \.self) { value in
				let range = (Double(value) / 10.0)...1.0
				ProgressView()
					.frame(height: 10)
					.progressViewStyle(RangedProgressView(range: range, bacgroundColor: .blue, foregroundColors: [.green, .yellow, .orange, .red]))
					.padding()
			}
			ProgressView(value: 0.5)
				.frame(height: 10)
				.progressViewStyle(RangedProgressView(range: 0.1...0.6, bacgroundColor: .blue, foregroundColors: [.green, .yellow, .orange, .red]))
				   .padding()
			ProgressView(value: 0.5)
				.frame(height: 10)
				.progressViewStyle(RangedProgressView(range: 0.3...0.75, bacgroundColor: .blue, foregroundColors: [.green, .yellow, .orange, .red]))
				   .padding()
			ProgressView(value: 0.67)
				.frame(height: 10)
				.progressViewStyle(RangedProgressView(range: 0.47...0.96, bacgroundColor: .blue, foregroundColors: [.green, .yellow, .orange, .red]))
				   .padding()
		}
	}
}
