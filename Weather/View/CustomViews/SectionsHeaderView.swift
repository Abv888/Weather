//
//  SectionsHeaderView.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 29.09.2023.
//

import SwiftUI

struct SectionsHeaderView: View {
	let systemImageName: String?
	let headerText: String?
	
    var body: some View {
		HStack {
			systemImageName.map {
				Image(systemName: $0)
			}
			headerText.map {
				Text($0)
			}
		}
		.font(.system(size: 14))
		.foregroundColor(.white.opacity(0.6))
		.fontWeight(.medium)
    }
}

#Preview {
	SectionsHeaderView(systemImageName: "calendar",
					   headerText: "10-DAY FORECAST")
	.background(.blue)
}
