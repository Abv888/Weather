//
//  DailyForecastView.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 22.09.2023.
//

import SwiftUI

struct DailyForecastView: View {
    var body: some View {
		VStack(alignment: .leading, spacing: 8) {
			SectionsHeaderView(systemImageName: "calendar",
							   headerText: "10-DAY FORECAST")
			ForEach(0..<10) { index in
				Divider()
				HStack {
					Text("Today")
					Spacer()
					Image(systemName: "cloud.fill")
					Spacer()
					HStack(spacing: 10) {
						Text("55°")
							.foregroundColor(.white.opacity(0.6))
						ProgressView(value: 0.64)
							.frame(maxWidth: 100, maxHeight: 4)
							.progressViewStyle(RangedProgressView(range: 0.2...0.85, bacgroundColor: .blue, foregroundColors: [.green, .yellow, .orange, .red]))
						Text("72°")
					}
				}
			}
		}
		.font(.system(size: 20))
		.fontWeight(.semibold)
		.foregroundColor(.white)
		.padding()
		.background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 16))
    }
}

#Preview {
    ScrollView {
    	DailyForecastView()
			.padding()
    }
	.background(.blue)
}
