//
//  HourlyForecastView.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 20.09.2023.
//

import SwiftUI

struct HourlyForecastView: View {
	
	@StateObject var viewModel = HourlyForecastViewModel()
	
    var body: some View {
		VStack(alignment: .leading, spacing: 10) {
			Text(viewModel.data.currentWeatherSummary)
                .font(.system(size: 14))
                .fontWeight(.medium)
            Divider()
            ScrollView(.horizontal) {
                HStack(spacing: 24) {
					ForEach(viewModel.data.hourlySnapshots, id: \.time) { weatherSnapshot in
                        VStack(alignment: .center, spacing: 10) {
							Text("\(weatherSnapshot.time)")
                                .font(.system(size: 14))
                                .fontWeight(.semibold)
							weatherSnapshot.condition.weatherImage
								.font(.system(size: 20))
							Text(weatherSnapshot.temperature.celsiusString)
                                .font(.system(size: 22))
                                .fontWeight(.medium)
                        }
                    }
                }
            }
			.scrollIndicators(.never)
        }
		.padding(16)
        .foregroundColor(.white)
		.background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 16))
		
    }
}

#Preview {
    ScrollView {
        HourlyForecastView()
    }
    .background(.blue)
}
