//
//  UVIndexView.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 29.09.2023.
//

import SwiftUI

struct UVIndexView: View {
	
	@StateObject var viewModel = UVIndexViewModel()
	
    var body: some View {
		WeatherWidgetView(data: WeatherWidgetModel(headerSystemImageName: "sun.max.fill",
												   headerText: "UV Index",
												   mainInfo: "\(viewModel.data.uvIndexValue)",
												   supportInfo: viewModel.data.indexRating,
												   information: viewModel.data.recomendation)) {
			ProgressView(value: viewModel.data.progressViewValue)
				.progressViewStyle(RangedProgressView(range: 0...1.0,
													  bacgroundColor: .gray,
													  foregroundColors: [.blue]))
				.frame(maxHeight: 10)
		}
												   .padding()
												   .background(.blue)
    }
}

#Preview {
    ScrollView {
    	UVIndexView()
    }
	.background(.blue)
}
