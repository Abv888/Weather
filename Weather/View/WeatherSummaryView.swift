//
//  WeatherSummaryView.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 20.09.2023.
//

import SwiftUI

struct WeatherSummaryView: View {
	
	@StateObject var viewModel = WeatherSummaryViewModel()
	
	var body: some View {
		VStack {
			Text(viewModel.data.locationCityName)
				.font(.system(size: 32))
			Text(viewModel.data.currentTemperature.celsiusString)
				.font(.system(size: 100))
				.fontWeight(.thin)
			VStack {
				Text(viewModel.data.weatherCondition.description)
				HStack {
					Text("H:") +  Text(viewModel.data.highestDayTemperature.celsiusString)
					Text("L:") +  Text(viewModel.data.lowestDayTemperature.celsiusString)
				}
			}
			.font(.system(size: 18))
			.fontWeight(.medium)
		}
		.shadow(radius: 4)
		.foregroundColor(.white)
	}
}

#Preview {
	ScrollView {
		HStack {
			Spacer()
			WeatherSummaryView()
			Spacer()
		}
	}
	.padding(.top, 60)
	.background(.blue)
}
