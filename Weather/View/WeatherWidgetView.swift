//
//  WeatherWidgetView.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 30.09.2023.
//

import SwiftUI

struct WeatherWidgetView<Content>: View where Content: View {
	let data: WeatherWidgetModel
	let content: Content
	
	init(data: WeatherWidgetModel, @ViewBuilder content: @escaping () -> Content) {
		self.data = data
		self.content = content()
	}
	
	var body: some View {
		VStack(alignment: .leading) {
			SectionsHeaderView(systemImageName: data.headerSystemImageName,
							   headerText: data.headerText?.uppercased())
			data.mainInfo.map {
				Text($0)
					.font(.system(size: 32))
			}
			data.supportInfo.map {
				Text($0)
					.font(.system(size: 18))
					.fontWeight(.semibold)
			}
			content
			data.information.map {
				Text($0)
			}
		}
		.foregroundColor(.white)
		.padding()
		.background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 16))
	}
}

#Preview {
	WeatherWidgetView(data: WeatherWidgetModel(headerSystemImageName: "sun.max.fill",
											   headerText: "UV Index",
											   mainInfo: "2",
											   supportInfo: "Low",
											   information: "Low for the rest of the day.")) {
		ProgressView(value: 0)
			.progressViewStyle(RangedProgressView(range: 0...1.0,
												  bacgroundColor: .gray,
												  foregroundColors: [.blue]))
			.frame(maxHeight: 10)
	}
											   .padding()
											   .background(.blue)
}
