//
//  HourlyForecastViewModel.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 28.09.2023.
//

import Foundation

class HourlyForecastViewModel: ObservableObject {
	let data: HourlyForecastModel
	
	init() {
		let currentDate = Date.now
		self.data = HourlyForecastModel(currentWeatherSummary: "Sunny conditions will continue for the rest of he day. Wind gusts are up to 18 mph.",
										hourlySnapshots: [WeatherSnapshot(time: currentDate.addingTimeInterval(60*60),
																		  condition: .cloudy,
																		  temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60),
																							condition: .cloudy,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*2),
																							condition: .partlyCloudy,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*3),
																							condition: .sleet,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*4),
																							condition: .clear,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*5),
																							condition: .rain,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*6),
																							condition: .haze,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*7),
																							condition: .snow,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*8),
																							condition: .drizzle,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*10),
																							condition: .drizzleNight,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*11),
																							condition: .clearNight,
																							temperature: .C(22)),
														  WeatherSnapshot(time: currentDate.addingTimeInterval(60*60*12),
																							condition: .clearNight,
																							temperature: .C(22))])
	}
}
