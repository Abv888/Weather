//
//  UVIndexViewModel.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 29.09.2023.
//

import Foundation

class UVIndexViewModel: ObservableObject {
	let data: UVIndexModel
	
	init() {
		self.data = UVIndexModel(uvIndexValue: 0,
								 indexRating: "Low",
								 progressViewValue: 0,
								 recomendation: "Use sun protection 11AM-4PM.")
	}
	
}
