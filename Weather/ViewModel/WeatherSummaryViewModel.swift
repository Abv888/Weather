//
//  WeatherSummaryViewModel.swift
//  Weather
//
//  Created by Bagrat Arutyunov on 27.09.2023.
//

import Foundation

class WeatherSummaryViewModel: ObservableObject {
	let data: WeatherSummaryModel
	
	init() {
		self.data = WeatherSummaryModel(locationCityName: "Moscow", currentTemperature: .C(10), weatherCondition: .mostlyClear, lowestDayTemperature: .C(5), highestDayTemperature: .C(15))
	}
	
}
