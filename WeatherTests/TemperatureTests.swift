//
//  TemperatureTests.swift
//  WeatherTests
//
//  Created by Bagrat Arutyunov on 28.09.2023.
//

import XCTest
@testable import Weather

final class TemperatureTests: XCTestCase {

    func testValueInC() throws {
		XCTAssertEqual(Temperature.C(10).valueInFahrenheit, 50)
		XCTAssertEqual(Temperature.C(10).valueInCelsius, 10)
    }
	
	func testValueInF() throws {
		XCTAssertEqual(Temperature.F(68).valueInCelsius, 20)
		XCTAssertEqual(Temperature.F(68).valueInFahrenheit, 68)
	}
	
	func testCelsiusString() throws {
		XCTAssertEqual(Temperature.C(10).fahrenheitString, "50°")
		XCTAssertEqual(Temperature.C(10).celsiusString, "10°")
	}
	
	func testFahrenheitString() throws {
		XCTAssertEqual(Temperature.F(68).fahrenheitString, "68°")
		XCTAssertEqual(Temperature.F(68).celsiusString, "20°")
	}

}
